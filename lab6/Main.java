public class Main{
    public static void main(String[] args){

        Point p = new Point(4,6);
        Rectangle r = new Rectangle(p,5,9);

        System.out.println(r.area());
        System.out.println(r.perimeter());

        Point[] points = r.corners();
        for(int i = 0; i < points.length; i++) {
            Point p = points[i];
            System.out.println(p.xCoord +" " + p.yCoord);
        }
    }
}