import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		int total = 0;
		int guess;  //Read the user input
        
        do{
            System.out.println("Type -1 to quit or give me a guess: ");
            guess = reader.nextInt();
            total++;
            if (guess == -1)
                System.out.println("Sorry, the number was " + number);
            else if (guess == number)
                System.out.println("Congratulations! You won after "+total+" attempts!");
            else if (guess > number)
                System.out.println("Mine ise less than your guess.");
            else if (guess < number)
                System.out.println("Mine is greater than your guess.");
            else
                System.out.println("Sorry");
        
        
       }while(guess != number && guess != -1); 
        
 	}

}
