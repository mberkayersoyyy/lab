public class FindPrimes{
    
   public static void main(String[] args){
        int number = Integer.parseInt(args[0]);
        int value = 2;
        String primeS = "";
        while(value < number){
            if(isPrime(value))
                primeS = primeS + "," + value;
            value++;
        }
        System.out.println(primeS.substring(1,primeS.length()));
    }


   public static boolean isPrime(int x){
        int divisor = 2;
        if(x == 2)
            return true;
        while(divisor < x){
            if(x % 2 == 0)
                return false;
            else if(x % divisor == 0)
                return false;
            else if(x % divisor != 0)
                    divisor++;
        }
        return true;
    }
    
}











